var dsnv =[];
var dsnvJson = localStorage.getItem("DSNV");
if(dsnvJson != null){
   var nvArr = JSON.parse(dsnvJson);
   dsnv = nvArr.map(function(item){
   return new NhanVien(item.taiKhoan,
    item.ten,item.email,item.matKhau,item.ngayLam,item.luongCB,
    item.chucVu,item.gioLam
    )
   })
    renderDSNV(dsnv);
};
function themNhanVien(){
    var nv = layThongTinNhanVien();
    var isValid = true;
    isValid = kiemTratrung(nv.taiKhoan,dsnv) && kiemTraDinhDang(nv.taiKhoan,"tbTKNV",4,6);
    isValid = isValid & kiemTraEmail(nv.email,"tbEmail") & kiemTraNgay(nv.ngayLam,"tbNgay")
& kiemTraGioLam(nv.gioLam,"tbGiolam") & kiemTraLuongCb(nv.luongCB,"tbLuongCB")
& kiemTraChucVu(nv.chucVu,"tbChucVu") &kiemTraChu(nv.ten,"tbTen")& kiemTraDinhDang(nv.matKhau,"tbMatKhau",6,10)
& kiemTraPass(nv.matKhau,"tbMatKhau")
    ;
    if(isValid){
        dsnv.push(nv);
        var dsnvJson = JSON.stringify(dsnv);
        localStorage.setItem("DSNV",dsnvJson);
        
    }
    renderDSNV(dsnv);
};


function xoaNhanVien(idNv){
var viTri = timKiemViTri(idNv,dsnv);
if(viTri != -1){
dsnv.splice(viTri,1);

var dsnvJson = JSON.stringify(dsnv);
localStorage.setItem("DSNV",dsnvJson);
renderDSNV(dsnv);


}
}

function suaNhanVien(idNv){
        var viTri = timKiemViTri(idNv,dsnv);
        if(viTri == -1){
            return; 
        }
        var nv = dsnv[viTri];
 showThongTinLenForm(nv);        
};

function capNhapNhanVien(){
var nv = layThongTinNhanVien();
var viTri = timKiemViTri(nv.taiKhoan,dsnv);
if(viTri != -1){
dsnv[viTri]=nv;

var dsnvJson = JSON.stringify(dsnv);
localStorage.setItem("DSNV",dsnvJson);

renderDSNV(dsnv);
}
}
