function kiemTratrung(idNv,nvArr){
var index = nvArr.findIndex(function(item){
    return idNv == item.taiKhoan;
})
if(index == -1){
    document.getElementById("tbTKNV").innerText="";
    return true;
}
else{
    document.getElementById("tbTKNV").innerText=`Tài khoản này đã tồn tại!`
    return false;
}
}

function kiemTraDinhDang(value,item,min,max){
var length = value.length;
if( length < min || length >max){
document.getElementById(item).innerText=`Độ dài phải từ ${min} đến ${max} kí tự!`
return false;
}else{
    document.getElementById(item).innerText="";
    return true;
} 
};
function kiemTraEmail(value,item){
    var reg =  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  var isEmail = reg.test(value);
  if(isEmail){
    document.getElementById(item).innerText="";
   return true;  
}
  else{
    document.getElementById(item).innerText=`Email sai định dạng!`;
    return false;  
  }
}
function kiemTraNgay(value,item){
    var reg = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
    var isCalendar = reg.test(value);
    if(isCalendar){
        document.getElementById(item).innerText="";
       return true;  
    }
      else{
        document.getElementById(item).innerText=`Ngày làm sai định dạng!`;
        return false;  
      }
};
function kiemTraGioLam(value,item){
if(value > 200 || value < 80){
    document.getElementById(item).innerText=` Giờ làm phải từ 80 đến 200 tiếng!`;
    return false;
}
else{
    document.getElementById(item).innerText="";
    return true;
}
};
function kiemTraLuongCb(value,item){
if(value < 1000000 || value > 20000000){
    document.getElementById(item).innerText=`Lương cơ bản phải từ 1 triệu đên 20 triệu!`;
    return false;
}
else{
    document.getElementById(item).innerText="";
    return true;
}
}

function kiemTraChucVu(value,item){
if(value =="Chọn chức vụ"){
    document.getElementById(item).innerText=`Vui lòng chọn chức vụ!`
    return false;
}else{
    document.getElementById(item).innerText="";
    return true;
}
}
function kiemTraChu(value,item){
var reg =/^\w\D+$/;
var isWord = reg.test(value);
if(isWord){
    document.getElementById(item).innerText="";
    return true;
}
else{
    document.getElementById(item).innerText=` Tên chỉ bao gồm chữ!`;
    return false;
}

}

function kiemTraPass(value,item){
    var reg = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,10}$/;
    var isPass = reg.test(value);
    if(isPass){
        document.getElementById(item).innerText="";
        return true;
    }
    else{
        document.getElementById(item).innerText=` Mật Khẩu phải chứa ít nhất (1 ký tự số, 1 ký tự in hoa, 1 ký tự đặc biệt!)`;
        return false;
    }  

}